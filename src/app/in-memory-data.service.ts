import { Injectable } from '@angular/core';
import { POKEMONS } from './pokemon/api-pokemon';
import { InMemoryDbService } from 'angular-in-memory-web-api';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService{

  private pokemonCounter = 13; // Initialisez le compteur d'ID à 13

  constructor() { }

  createDb() {
    let pokemons = POKEMONS;
    
    // Vérifie si les données sont présentes dans le stockage local
    const storedPokemons = localStorage.getItem('pokemons');
    if (storedPokemons) {
      pokemons = JSON.parse(storedPokemons);
    }

    return { pokemons };
  }

  // Méthode pour obtenir un nouvel ID de Pokémon
  private getNewPokemonId(): number {
    return this.pokemonCounter++;
  }

  // Méthode pour créer un nouveau Pokémon avec un ID incrémenté
  public createPokemon(newPokemon: any): any {
    const pokemonWithId = { ...newPokemon, id: this.getNewPokemonId() };
    POKEMONS.push(pokemonWithId);
    return pokemonWithId;
  }

}
