import { Component } from '@angular/core';
import { fadeAndSlideAnimation, flipAnimation, scaleAnimation, slideInAnimation } from './animations/animations';
import { RouterOutlet } from '@angular/router';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: `./app.component.html`,
  styles: [],
  animations: [flipAnimation]
})
export class AppComponent {
  isLoggedIn: boolean|undefined;
  constructor(public authService: AuthService) {}

  ngOnInit() {
    this.isLoggedIn = this.authService.isLoggedIn;
  }
  
  logout() {
    this.authService.logout();
  }
  
  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }
 }