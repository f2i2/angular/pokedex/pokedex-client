import { Injectable } from '@angular/core';
import { InMemoryDbService, RequestInfo, ResponseOptions, STATUS } from 'angular-in-memory-web-api';
import { POKEMONS } from './pokemon/api-pokemon';
import { Pokemon } from './pokemon/pokemon.models';

@Injectable({
  providedIn: 'root'
})
export class CustomInMemoryDbService extends InMemoryDbService {
  private pokemonCounter = 13; // Initialisez le compteur d'ID à 1

  constructor() {
    super();
  }

  // Surcharge de la méthode createDb
  createDb() {
      let pokemons = POKEMONS;
      
      // Vérifie si les données sont présentes dans le stockage local
      // const storedPokemons = localStorage.getItem('pokemons');
      // if (storedPokemons) {
      //   pokemons = JSON.parse(storedPokemons);
      // }

      // localStorage.setItem('pokemons', JSON.stringify(pokemons));
  
      return { pokemons };
    }

  // Méthode pour obtenir un nouvel ID de Pokémon
  private getNewPokemonId(): number {
    return this.pokemonCounter++;
  }

  // Surcharge de la méthode get pour vérifier les données dans le stockage local
  // get(reqInfo: RequestInfo) {
  //   if (reqInfo.collectionName === 'pokemons' && !reqInfo.query) {
  //     const storedPokemons = localStorage.getItem('pokemons');
  //     if (storedPokemons) {
  //       const parsedPokemons = JSON.parse(storedPokemons);
  //       POKEMONS.length = 0; // Supprimer les éléments actuels de POKEMONS
  //       parsedPokemons.forEach((pokemon: Pokemon) => {
  //         // Ajouter chaque pokemon dans la liste POKEMONS
  //         POKEMONS.push(pokemon);
  //       });
  //       return reqInfo.utils.createResponse$(() => {
  //         return {
  //           status: STATUS.OK,
  //           headers: reqInfo.headers,
  //           body: parsedPokemons
  //         };
  //       });
  //     } else {
  //       localStorage.setItem('pokemons', JSON.stringify(POKEMONS));
  //       return reqInfo.utils.createResponse$(() => {
  //         return {
  //           status: STATUS.OK,
  //           headers: reqInfo.headers,
  //           body: POKEMONS
  //         };
  //       });
  //     }
  //   } 
  
  //     return undefined; // Laissez la méthode get par défaut gérer les autres collections
  //   }

  // // Surcharge de la méthode post pour gérer les requêtes POST personnalisées
  post(reqInfo: RequestInfo) {
    if (reqInfo.collectionName === 'pokemons') {
      const pokemon = reqInfo.utils.getJsonBody(reqInfo.req);
      const pokemonWithId = { ...pokemon, id: this.getNewPokemonId() };
      POKEMONS.push(pokemonWithId);
      localStorage.setItem("pokemons", JSON.stringify(POKEMONS));
      return reqInfo.utils.createResponse$(() => {
        return {
          status: STATUS.OK,
          headers: reqInfo.headers,
          body: pokemonWithId
        };
      });
    }

    return undefined; // Laissez la méthode post par défaut gérer les autres collections
  }


  // put(reqInfo: RequestInfo) {
  //   if (reqInfo.collectionName === 'pokemons') {
  //     const pokemon = reqInfo.utils.getJsonBody(reqInfo.req);
  
  //     // Mettre à jour les données dans le stockage local
  //     let storedPokemons = localStorage.getItem('pokemons');
  //     let pokemons: any[] = [];
  //     if (storedPokemons) {
  //       pokemons = JSON.parse(storedPokemons);
  //     } else {
  //       // Initialiser les données à partir de POKEMONS si le localStorage est vide
  //       pokemons = POKEMONS;
  //     }
  
  //     const updatedPokemons = pokemons.map((p: any) => (p.id === pokemon.id ? pokemon : p));
  //     localStorage.setItem('pokemons', JSON.stringify(updatedPokemons));
  
  //     // Retourner une réponse vide ou un message de succès
  //     const options: ResponseOptions = {
  //       status: 200,
  //       body: JSON.stringify(null), // Corps de la réponse est une chaîne JSON null
  //       headers: reqInfo.headers,
  //       statusText: 'OK'
  //     };
  //     return reqInfo.utils.createResponse$(() => options);
  //   }
  
  //   return undefined; // Laisser la méthode par défaut gérer les autres collections
  // }
  
  
  

}
