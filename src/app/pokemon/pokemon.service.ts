import { Injectable } from "@angular/core";
import { Pokemon } from "./pokemon.models";
import { POKEMONS } from "./api-pokemon";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, catchError, map, of, tap } from "rxjs";
import { InMemoryDataService } from "../in-memory-data.service";
import { CustomInMemoryDbService } from "../custom-in-memory-data.service";

@Injectable({
  providedIn: "root",
})
export class PokemonService {
  constructor(private http: HttpClient, private inMemoryDataService: CustomInMemoryDbService) {}

  createPokemon(newPokemon: Pokemon|undefined): Observable<Pokemon | undefined> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    return this.http.post<Pokemon>('api/pokemons', newPokemon, httpOptions).pipe(
      tap((response) => console.table(response)),
      catchError((error) => {
        console.log(error);
        return of(undefined); // Retourne undefined en cas d'erreur
      })
    );
  }

  updatepokemon(updatedPokemon: Pokemon|undefined): Observable<null|undefined> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Conteny-Type': 'application/json' })
    }

    return this.http.put<null> (`api/pokemons`, updatedPokemon, httpOptions).pipe(
      tap((response) => console.table(response)),
      catchError((error) => {
        console.log(error);
        return of(undefined);
      }))
  }

  getTypesList(): string[] {
    return [
      "Feu",
      "Eau",
      "Plante",
      "Insecte",
      "Normal",
      "Vol",
      "Poison",
      "Fée",
      "Psy",
      "Electrik",
      "Combat",
    ];
  }

  getPokemonList(): Observable<Pokemon[]> {
    console.log("Appel de getPokemonList() depuis pokemonService");
    
    return this.http.get<Pokemon[]> ('api/pokemons').pipe(
      tap((response) => {
        console.table(response)
      }),
      catchError((error) => {
        console.log('Error ...')
        console.log(error);
        return of([]);
      }))
  }

  searchPokemonList(term: string): Observable<Pokemon[]> {
    console.log(`Recherche de Pokémon avec le terme: ${term}`);
    
    return this.http.get<Pokemon[]>(`api/pokemons/?name=${term}`).pipe(
      tap((response) => {
        console.table(response)
      }),
      catchError((error) => {
        console.log('Error ...')
        console.log(error);
        return of([]);
      }))
  }



  // updatePokemonList(pokemon: Pokemon | undefined) {
  //   if (pokemon != undefined) {
  //     // Mettez à jour le Pokémon dans la liste
  //     const index = POKEMONS.findIndex((p) => p.id === pokemon.id);
  //     if (index !== -1) {
  //       POKEMONS[index] = pokemon;
  //       // Mettez à jour le localStorage
  //       localStorage.setItem("pokemons", JSON.stringify(POKEMONS));
  //     }
  //   }
  // }

 
  getPokemonById(pokemonId: number): Observable<Pokemon|undefined> {
    return this.http.get<Pokemon|undefined> (`api/pokemons/${pokemonId}`).pipe(
      tap((response) => console.log(response)),
      catchError((error) => {
        console.log(error);
        return of(undefined);
      }))
  }

  getPokemonsFromLocalStorage(): Pokemon[] {
    const storedPokemons = localStorage.getItem('pokemons');
    if (storedPokemons) {
      try {
        return JSON.parse(storedPokemons);
      } catch (error) {
        console.error('Error parsing JSON from local storage:', error);
        return [];
      }
    } else {
      return [];
    }
  }
  
}
