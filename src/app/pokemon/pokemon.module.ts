import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BorderCardDirective } from './border-card.directive';
import { PokemonTypeColorPipe } from './pokemon-type-color.pipe';
import { ListPokemonComponent } from './list-pokemon/list-pokemon.component';
import { DetailPokemonComponent } from './detail-pokemon/detail-pokemon.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { PokemonFormComponent } from './pokemon-form/pokemon-form.component';
import { EditPokemonComponent } from './edit-pokemon/edit-pokemon.component';
import { AddPokemonComponent } from './add-pokemon/add-pokemon.component';
import { SearchPokemonComponent } from './search-pokemon/search-pokemon.component';
import { AuthGuard } from '../auth.guard';

const pokemonsRoutes: Routes = [
  {path: 'pokemons', component: ListPokemonComponent, data: { animation: 'pokemonList' }, canActivate:[AuthGuard]},
  {path: 'pokemon/:id', component: DetailPokemonComponent, data: { animation: 'pokemonDetail' }, canActivate:[AuthGuard]},
  {path: 'edit/pokemon/:id', component: EditPokemonComponent, canActivate:[AuthGuard]},
  {path: 'add/pokemon', component: AddPokemonComponent, canActivate:[AuthGuard]},

];

@NgModule({
  declarations: [
    BorderCardDirective,
    PokemonTypeColorPipe,
    ListPokemonComponent,
    DetailPokemonComponent,
    PokemonFormComponent,
    EditPokemonComponent,
    AddPokemonComponent,
    SearchPokemonComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(pokemonsRoutes)
  ]
})
export class PokemonModule { }
