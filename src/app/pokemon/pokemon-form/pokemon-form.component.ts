import { Component, Input } from '@angular/core';
import { Pokemon } from '../pokemon.models';
import { PokemonService } from '../pokemon.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-pokemon-form',
  templateUrl: `./pokemon-form.component.html`,
  styleUrls: [
    `./pokemon-form.component.css`,
  ]
})
export class PokemonFormComponent { 

  @Input() pokemon: Pokemon | undefined;
  types: string[] = [];
  @Input() isAddPokemonForm: boolean = false;
  submitText: string = "";
  private createPokemonSubscription: Subscription | undefined;
  private updatePokemonSubscription: Subscription | undefined;


  constructor(
    public pokemonService: PokemonService, 
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.types = this.pokemonService.getTypesList();
    if (this.isAddPokemonForm) {
      this.submitText = "Ajouter le Pokémon";
      if(this.pokemon){
        this.pokemon.picture = this.generateRandomPokemonImage();
      }
    } else {
      this.submitText = "Enregistrer les modifications"
    }
  }

  generateRandomPokemonImage(): string {
    // Générer une URL aléatoire pour l'image de Pokémon
    return 'https://lorempokemon.fakerapi.it/pokemon/200/' + (200 + Math.floor(Math.random() * 1000));
  }

  /** Cette fonction permet de vérifier si le pokemon possède le type passé en paramètre */
  hasType(type: string): boolean {
    return this.pokemon?.types.includes(type) || false;
  }

  /* Cette fonction permet d'ajouter le type passé en paramètre sur le pokémon en cours d'édition, si le typ était déjà affecté au pokémon, il sera retiré de celui ci */
  selectType($event: Event, type: string): void {
    const isChecked: boolean = ($event.target as HTMLInputElement).checked;

    if (isChecked){
      this.pokemon?.types.push(type)
    } else {
      const index = this.pokemon?.types.indexOf(type);
      if(index) {
        this.pokemon?.types.splice(index, 1);
      }
    }
  }

  onSubmit() {
    if(this.isAddPokemonForm) {
      this.createPokemonSubscription = this.pokemonService.createPokemon(this.pokemon).subscribe((createdPokemon) => {
        this.pokemon = createdPokemon;
        // this.pokemonService.updatePokemonList(this.pokemon);
        this.router.navigate(['/pokemons'], { replaceUrl: true });
        console.log("Pokémon cree:", createdPokemon);
        
      })
    } else {
      this.updatePokemonSubscription = this.pokemonService.updatepokemon(this.pokemon).subscribe(() => {
          // this.pokemonService.updatePokemonList(this.pokemon);
          this.router.navigate(['/pokemons'], { replaceUrl: true });
        })
    }
  }

  ngOnDestroy() {
    this.pokemon =  new Pokemon();
    // S'assurer de se désabonner pour éviter les fuites de mémoire
    if (this.createPokemonSubscription) {
      this.createPokemonSubscription.unsubscribe();
    }
    if (this.updatePokemonSubscription) {
      this.updatePokemonSubscription.unsubscribe();
    }
  }
}