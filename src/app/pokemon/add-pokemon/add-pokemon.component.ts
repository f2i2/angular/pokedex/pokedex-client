import { Component } from '@angular/core';
import { Pokemon } from '../pokemon.models';

@Component({
  selector: 'app-add-pokemon',
  template: `
  <div *ngIf="pokemon">
    <h2 class="text-center">{{ pokemon.name }}</h2>
    <div class="text-center">
      <img [src]="pokemon.picture">
    </div>
    <app-pokemon-form [pokemon]="pokemon" [isAddPokemonForm]="true"></app-pokemon-form>
  </div>
  `,
  styles: [
  ]
})
export class AddPokemonComponent {
  pokemon: Pokemon|undefined = new Pokemon();
}
